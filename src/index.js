import ReactDOM from "react-dom";
import React from "react";
import ReactCrop from "react-image-crop";
import "react-image-crop/dist/ReactCrop.css";

import "./App.css";

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      src: null,
      saved: false,
      isPreviewEnabled: ''
    };
    this.fileUpload = React.createRef();
  }

  onSelectFile = e => {
    if (e.target.files && e.target.files.length > 0) {
      if (Math.round(e.target.files[0].size/1024) >= 1024) {
        e.target.value = '';
        return alert('Image size is greater than 1 MB, Please select different image.');
      }
      const reader = new FileReader();
      reader.addEventListener("load", () =>
        this.setState({ src: reader.result })
      );
      reader.readAsDataURL(e.target.files[0]);
    }
  };

  // If you setState the crop in here you should return false.
  onImageLoaded = image => {
    this.imageRef = image;
  };

  onCropComplete = crop => {
    this.makeClientCrop(crop);
  };

  handleSaveImage = () => {
    if (this.state.croppedImageUrl) {
      const img = Promise.resolve(this.state.croppedImageUrl);
      img.then(url => { this.setState( { previewUrl: url, isPreviewEnabled: true }) });
    }
  };

  handlePrintPreview = () => {
    const w = window.open("");
    const printContent = "<div style='text-align: center;'><img height='100px' src="+this.state.previewUrl+" /></div>";
    w.document.write(printContent);
    var img = new Image();
    // onload fires when the image is fully loaded, and has width and height
    img.onload = function() {
      w.print();
      w.close();
    };
    // set attributes and src
    img.setAttribute('crossOrigin', 'anonymous'); //
    img.src = this.state.previewUrl;
  }

  handleClear = () => {
    this.fileUpload.current.value = ''
    this.setState({src: '', saved: false});
  }

  onCropChange = (crop) => {
    this.setState({ crop });
  };

  async makeClientCrop(crop) {
    if (this.imageRef && crop.width && crop.height) {
      const croppedImageUrl = await this.getCroppedImg(
        this.imageRef,
        crop,
        "newFile.jpeg"
      );
      this.setState({ croppedImageUrl });
    }
  }

  getCroppedImg(image, crop, fileName) {
    const canvas = document.createElement("canvas");
    const scaleX = image.naturalWidth / image.width;
    const scaleY = image.naturalHeight / image.height;
    canvas.width = crop.width;
    canvas.height = crop.height;
    const ctx = canvas.getContext("2d");

    ctx.drawImage(
      image,
      crop.x * scaleX,
      crop.y * scaleY,
      crop.width * scaleX,
      crop.height * scaleY,
      0,
      0,
      crop.width,
      crop.height
    );
    if((crop.width && crop.width > 800) || (crop.height && crop.height > 100)) {
      this.setState({isPreviewEnabled: ''});
      return window.alert("Cropping size exceeded the maximum limit!");
    }
    return new Promise((resolve, reject) => {
      canvas.toBlob(blob => {
        if (!blob) {
          console.error("Canvas is empty");
          return;
        }
        blob.name = fileName;
        window.URL.revokeObjectURL(this.fileUrl);
        this.fileUrl = window.URL.createObjectURL(blob);
        resolve(this.fileUrl);
      }, "image/jpeg");
    });
  }

  isDisabled() {
    return false;
  }

  render() {
    const { crop, src, isPreviewEnabled } = this.state;
    return (
      <div className="App">
        <div>
          <input type="file" onChange={this.onSelectFile} accept="image/*" ref={this.fileUpload} />
          <span className="actions-btn">
            <button onClick={this.handleClear}>Clear</button>
            <button onClick={this.handleSaveImage}>Save Image</button>
            <button disabled={!isPreviewEnabled} onClick={this.handlePrintPreview}>Print Preview</button>
          </span>
        </div>
        {src && (
          <ReactCrop
            src={src}
            crop={crop}
            onImageLoaded={this.onImageLoaded}
            onComplete={this.onCropComplete}
            onChange={this.onCropChange}
          />
        )}
      </div>
    );
  }
}

ReactDOM.render(<App />, document.getElementById("root"));